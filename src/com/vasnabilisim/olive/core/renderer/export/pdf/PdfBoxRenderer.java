package com.vasnabilisim.olive.core.renderer.export.pdf;

import java.io.IOException;
import java.util.TreeMap;

import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.olive.core.renderer.PageRenderer;
import com.vasnabilisim.olive.core.renderer.SimpleRenderer;
import com.vasnabilisim.util.ClassComparator;

/**
 * @author Menderes Fatih GUVEN
 */
public abstract class PdfBoxRenderer<R extends SimpleRenderer> {

	private static final TreeMap<Class<?>, PdfBoxRenderer<?>> rendererMap = new TreeMap<>(new ClassComparator());
	
	public static <R extends SimpleRenderer> void register(PdfBoxRenderer<R> pdfRenderer) {
		rendererMap.put(pdfRenderer.rendererClass(), pdfRenderer);
	}
	
	@SuppressWarnings("unchecked")
	public static <R extends SimpleRenderer> PdfBoxRenderer<R> get(Class<R> rendererClass) {
		return (PdfBoxRenderer<R>) rendererMap.get(rendererClass);
	}
	
	@SuppressWarnings("unchecked")
	public static <R extends SimpleRenderer> PdfBoxRenderer<R> get(R renderer) {
		return (PdfBoxRenderer<R>) get(renderer.getClass());
	}
	
	static {
		register(new PdfBoxLineRenderer());
	}
	
	protected PdfBoxRenderer() {
	}
	
	/**
	 * Returns the class pdf-rendered by this.
	 * @return
	 */
	public abstract Class<R> rendererClass();

	/**
	 * Pdf-renders the given renderer to given content stream.
	 * @param contentStream
	 * @param pageRenderer
	 * @param renderer
	 * @throws IOException
	 * @throws BaseException
	 */
	public abstract void render(
			PDPageContentStream contentStream, 
			PageRenderer pageRenderer, 
			R renderer) throws IOException, BaseException;

}
