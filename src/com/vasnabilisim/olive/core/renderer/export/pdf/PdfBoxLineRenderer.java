package com.vasnabilisim.olive.core.renderer.export.pdf;

import java.io.IOException;

import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.olive.core.renderer.LineRenderer;
import com.vasnabilisim.olive.core.renderer.PageRenderer;

/**
 * @author Menderes Fatih GUVEN
 */
public class PdfBoxLineRenderer extends PdfBoxRenderer<LineRenderer> {

	@Override
	public Class<LineRenderer> rendererClass() {
		return LineRenderer.class;
	}
	
	@Override
	public void render(
			PDPageContentStream contentStream, 
			PageRenderer pageRenderer, 
			LineRenderer renderer)
			throws IOException, BaseException 
	{
		contentStream.setStrokingColor(PdfBoxUtil.toAwtColor(renderer.getColor()));
		contentStream.setLineWidth(renderer.getThickness());
		contentStream.drawLine(renderer.getFromPoint().x, 
				renderer.getFromPoint().y, 
				renderer.getToPoint().x, 
				renderer.getToPoint().y);
	}
}
