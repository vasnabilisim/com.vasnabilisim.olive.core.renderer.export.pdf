package com.vasnabilisim.olive.core.renderer.export.pdf;

import org.apache.pdfbox.pdmodel.common.PDRectangle;

import com.vasnabilisim.olive.core.paper.Paper;
import com.vasnabilisim.olive.core.paper.PaperUnit;

/**
 * @author Menderes Fatih GUVEN
 */
public final class PdfBoxUtil {

	/**
	 * Private default constructor.
	 */
	private PdfBoxUtil() {
	}

	/**
	 * Cm value of one inch.
	 */
	public static float INCH_CM	= 2.54f;

	/**
	 * inch value of one cm.
	 */
	public static float CM_INCH	= 1f / INCH_CM; //0.393701f;
	
	/**
	 * Number of pixels in one inch.
	 */
	public static float INCH_PIXEL = 72f;
	
	/**
	 * Number of pixels in one cm.
	 */
	public static float CM_PIXEL = INCH_PIXEL * CM_INCH; //28.346456692913385826771653543307f;

	/**
	 * Converts given paper to PDRectangle.
	 * @param paper
	 * @return
	 */
	public static PDRectangle toPDRectangle(Paper paper) {
		if(paper.getUnit() == PaperUnit.inch) 
			return new PDRectangle(paper.getOrientedWidth() * INCH_PIXEL, paper.getOrientedHeight() * INCH_PIXEL);
		else 
			return new PDRectangle(paper.getOrientedWidth() * CM_PIXEL, paper.getOrientedHeight() * CM_PIXEL);
	}
	
	/**
	 * Converts given color to awt color.
	 * @param color
	 * @return
	 */
	public static java.awt.Color toAwtColor(com.vasnabilisim.olive.core.paint.Color color) {
		return new java.awt.Color(color.r, color.g, color.b, color.o);
	}
	
}
