package com.vasnabilisim.olive.core.renderer.export.pdf;

import java.awt.Graphics2D;
import java.io.OutputStream;

import com.itextpdf.awt.PdfGraphics2D;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfWriter;
import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.core.ErrorException;
import com.vasnabilisim.olive.core.paper.Paper;
import com.vasnabilisim.olive.core.paper.PaperUnit;
import com.vasnabilisim.olive.core.renderer.PageRenderer;
import com.vasnabilisim.olive.core.renderer.ReportRenderer;
import com.vasnabilisim.olive.core.renderer.export.GraphicsExporter;

/**
 * @author Menderes Fatih GUVEN
 */
public class ITextPdfExporter extends GraphicsExporter {

	Document pdfDocument;
	PdfWriter pdfWriter;
	PdfGraphics2D pdfGraphics;
	
	/**
	 * Default constructor.
	 */
	public ITextPdfExporter() {
	}

	/**
	 * @see com.vasnabilisim.olive.core.renderer.export.Exporter#name()
	 */
	@Override
	public String name() {
		return "Pdf";
	}

	/**
	 * @see com.vasnabilisim.olive.core.renderer.export.GraphicsExporter#begin(java.io.OutputStream, com.vasnabilisim.olive.core.renderer.ReportRenderer)
	 */
	@Override
	protected void begin(OutputStream out, ReportRenderer reportRenderer) throws BaseException {
		Paper paper = reportRenderer.getPaper();
		Rectangle pdfRectangle = toRectangle(paper);
		pdfDocument = new Document(
			pdfRectangle, 
			toPixels(paper.getUnit(), paper.getOrientedLeftMargin()), 
			toPixels(paper.getUnit(), paper.getOrientedRightMargin()), 
			toPixels(paper.getUnit(), paper.getOrientedTopMargin()), 
			toPixels(paper.getUnit(), paper.getOrientedBottomMargin())
		);
		try {
			pdfWriter = PdfWriter.getInstance(pdfDocument, out);
		} catch (DocumentException e) {
			throw new ErrorException(e);
		}
		pdfDocument.open();
		pdfDocument.addTitle("VasnaBilisim Olive Reports Pdf Export");
		pdfDocument.addAuthor("VasnaBilisim Olive Reports");
		pdfDocument.addCreator("VasnaBilisim Olive Reports");
		
		PdfContentByte pdfContent = pdfWriter.getDirectContent();
		pdfGraphics = new PdfGraphics2D(pdfContent, pdfRectangle.getWidth(), pdfRectangle.getHeight());
	}
	
	/**
	 * @see com.vasnabilisim.olive.core.renderer.export.GraphicsExporter#createGraphics(com.vasnabilisim.olive.core.renderer.PageRenderer, int)
	 */
	@Override
	protected Graphics2D createGraphics(
			PageRenderer pageRenderer,
			int pageIndex) throws BaseException
	{
		if(pageIndex >= 0)
			pdfDocument.newPage();
		return pdfGraphics;
	}

	/**
	 * @see com.vasnabilisim.olive.core.renderer.export.GraphicsExporter#end()
	 */
	protected void end() throws BaseException {
		pdfGraphics.dispose();
		pdfDocument.close();
		pdfDocument = null;
		pdfWriter = null;
	}
	
	/**
	 * @see com.vasnabilisim.olive.core.renderer.export.GraphicsExporter#toPixels(com.vasnabilisim.olive.core.paper.PaperUnit, float)
	 */
	protected float toPixels(PaperUnit unit, float value) {
		if(unit == PaperUnit.inch)
			return value * INCH_PIXEL;
		return value * CM_PIXEL;
	}
	
	Rectangle toRectangle(Paper paper) {
		return new Rectangle(
				toPixels(paper.getUnit(), paper.getOrientedWidth()), 
				toPixels(paper.getUnit(), paper.getOrientedHeight())
				);
	}

	/**
	 * Cm value of one inch.
	 */
	public static float INCH_CM	= 2.54f;

	/**
	 * inch value of one cm.
	 */
	public static float CM_INCH	= 1f / INCH_CM; //0.393701f;
	
	/**
	 * Number of pixels in one inch.
	 */
	public static float INCH_PIXEL = 72f;
	
	/**
	 * Number of pixels in one cm.
	 */
	public static float CM_PIXEL = INCH_PIXEL * CM_INCH; //28.346456692913385826771653543307f;
}
