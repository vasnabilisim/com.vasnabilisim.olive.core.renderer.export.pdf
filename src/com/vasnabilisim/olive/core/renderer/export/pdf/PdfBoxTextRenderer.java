package com.vasnabilisim.olive.core.renderer.export.pdf;

import java.io.IOException;

import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.olive.core.renderer.PageRenderer;
import com.vasnabilisim.olive.core.renderer.TextRenderer;

/**
 * @author Menderes Fatih GUVEN
 */
public class PdfBoxTextRenderer extends PdfBoxRenderer<TextRenderer> {

	@Override
	public Class<TextRenderer> rendererClass() {
		return TextRenderer.class;
	}
	
	@Override
	public void render(
			PDPageContentStream contentStream, 
			PageRenderer pageRenderer, 
			TextRenderer renderer)
			throws IOException, BaseException 
	{
	}
}
