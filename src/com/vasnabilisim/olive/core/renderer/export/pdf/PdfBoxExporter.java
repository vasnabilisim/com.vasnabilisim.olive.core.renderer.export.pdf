package com.vasnabilisim.olive.core.renderer.export.pdf;

import java.io.IOException;
import java.io.OutputStream;

import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.core.ErrorException;
import com.vasnabilisim.core.Logger;
import com.vasnabilisim.olive.core.renderer.PageRenderer;
import com.vasnabilisim.olive.core.renderer.ReportRenderer;
import com.vasnabilisim.olive.core.renderer.SimpleRenderer;
import com.vasnabilisim.olive.core.renderer.export.Exporter;

/**
 * @author Menderes Fatih GUVEN
 */
public class PdfBoxExporter implements Exporter {

	public PdfBoxExporter() {
	}

	/**
	 * @see com.vasnabilisim.olive.core.renderer.export.Exporter#name()
	 */
	@Override
	public String name() {
		return "Pdf";
	}

	/**
	 * @see com.vasnabilisim.olive.core.renderer.export.Exporter#export(com.vasnabilisim.olive.core.renderer.ReportRenderer, java.io.OutputStream)
	 */
	@Override
	public void export(ReportRenderer reportRenderer, OutputStream out) throws BaseException {
		try(PDDocument document = new PDDocument()) {
			PDRectangle rectangle = PdfBoxUtil.toPDRectangle(reportRenderer.getPaper());
			for(PageRenderer pageRenderer : reportRenderer.getPages()) {
				PDPage page = new PDPage(rectangle);
				try (PDPageContentStream contentStream = new PDPageContentStream(document, page)) {
					export(pageRenderer, contentStream);
				} catch (IOException e) {
					Logger.log(e);
					throw new ErrorException("Unable to export to pdf.", e);
				}
				document.addPage(page);
			}
			document.save(out);
		} catch (COSVisitorException | IOException e) { 
			Logger.log(e);
			throw new ErrorException("Unable to export to pdf.", e);
		}
	}

	private void export(PageRenderer pageRenderer, PDPageContentStream contentStream) throws BaseException, IOException {
		for(SimpleRenderer simpleRenderer : pageRenderer.getChildren()) {
			PdfBoxRenderer<SimpleRenderer> pdfRenderer = PdfBoxRenderer.get(simpleRenderer);
			pdfRenderer.render(contentStream, pageRenderer, simpleRenderer);
		}
	}
}
